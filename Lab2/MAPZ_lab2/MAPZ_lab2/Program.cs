﻿/*public interface IAnimal
{
    void Eat();
    void Sleep();
}

public abstract class Animal : IAnimal
{
    public string Name { get; set; }
    public int Age { get; set; }

    public virtual void Eat()
    {
        Console.WriteLine($"{Name} is eating.");
    }

    public virtual void Sleep()
    {
        Console.WriteLine($"{Name} is sleeping.");
    }

    public abstract void Move();
}

public class Dog : Animal
{
    public string Breed { get; set; }

    public override void Move()
    {
        Console.WriteLine($"{Name} ({Breed}) is running.");
    }
}


public class Task2
{
    public int publicVar;
    private int privateVar;
    protected int protectedVar;
    private protected int privateProtectedVar;
    internal int internalVar;
    internal protected int internalProtectedVar;
}

public class Task2Child : Task2
{
    public void changeVar()
    {
        privateVar = 1;
        protectedVar = 2;
        privateProtectedVar = 3;
        publicVar = 4;
        internalVar = 5;
        internalProtectedVar = 6;
    }
}


class Program
{
    static void Main()
    {
        Task2 task2 = new Task2();

        task2.privateVar = 1;
        task2.protectedVar = 2;
        task2.privateProtectedVar = 3;
        task2.publicVar = 4;
        task2.internalVar = 5;
        task2.internalProtectedVar = 6;






        Dog dog = new Dog
        {
            Name = "Buddy",
            Age = 5,
            Breed = "Labrador Retriever"
        };
        dog.Eat();
        dog.Sleep();
        dog.Move();
    }
}
*/