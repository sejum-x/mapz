﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11 {
    class Converter
    {
        private float value;

        public Converter(float value)
        {
            this.value = value;
        }

        public static explicit operator int(Converter converter)
        {
            return (int)converter.value;
        }

        public static implicit operator double(Converter converter)
        {
            return converter.value;
        }
    }

    class Program
    {
        static void Main()
        {
            Converter converter = new Converter(10.5f);

            int intValue = (int)converter; 
            Console.WriteLine("Value as int: " + intValue);

            double doubleValue = converter; 
            Console.WriteLine("Value as double: " + doubleValue);
        }
    }
}
