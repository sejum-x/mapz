﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class NoAccessClass
    {
        int var1;
        int var2 {  get; set; }
        void method() { }

        class InsertedClass
        {
            int var3;
        }
    }

    class Child : NoAccessClass
    {
        public void change()
        {
            var1 = 1;
            var2 = 2;
            method();
            InsertedClass.var3 = 3;
        }
    }

    struct NoAccessStruct
    {
        int var;
        void method() { }
    }

    interface INoAccessInterfce
    {
        void InterfaceMethod();
    }

    class InterfaceRealize : INoAccessInterfce
    {
        public void InterfaceMethod() {
            Console.WriteLine("hto trymae cej rayon? PES PATRON ");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            NoAccessClass noAccessClass = new NoAccessClass();
            noAccessClass.var1 = 1;
            noAccessClass.var2 = 2;
            noAccessClass.method();
            noAccessClass.InsertedClass.var3 = 3;


            NoAccessStruct noAccessStruct = new NoAccessStruct();
            noAccessStruct.var = 1;
            noAccessStruct.method();
        }
    }

}
*/