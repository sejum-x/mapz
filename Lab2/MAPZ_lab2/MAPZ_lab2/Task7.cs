﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    class BaseClass
    {
        private int baseValue;

        public BaseClass()
        {
            Console.WriteLine("BaseClass default constructor called");
            baseValue = 0;
        }

        public BaseClass(int value)
        {
            Console.WriteLine("BaseClass parameterized constructor called with value: " + value);
            baseValue = value;
        }

        public int GetBaseValue()
        {
            return baseValue;
        }
    }

    class DerivedClass : BaseClass
    {
        private int derivedValue;

        public DerivedClass() : base()
        {
            Console.WriteLine("DerivedClass default constructor called");
            this.derivedValue = 0;
        }

        public DerivedClass(int baseValue, int derivedValue) : base(baseValue)
        {
            Console.WriteLine("DerivedClass parameterized constructor called with baseValue: " + baseValue + " and derivedValue: " + derivedValue);
            this.derivedValue = derivedValue;
        }

        public int GetDerivedValue()
        {
            return derivedValue;
        }

        public void OverloadedMethod()
        {
            Console.WriteLine("OverloadedMethod without parameters");
        }

        public void OverloadedMethod(int value)
        {
            Console.WriteLine("OverloadedMethod with parameter: " + value);
        }
    }
}
*/