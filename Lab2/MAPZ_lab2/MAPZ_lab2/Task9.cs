﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{    class Program
    {
        static void UpdStrClear(string str)
        {
            str += " update";
            Console.WriteLine($"Srting in clear function: {str} \n");
        }

        static void UpdStrOut(string str1, string str2, out string str3)
        {
            str3 = str1 + " " + str2; 
        }

        static void UpdStrRef(ref string str)
        {
            str += " UpdStrRef";
        }

        *//*static string UpdStrNoOut(out string str)
        {
            string x = "4";
            string y = "8";

            return x + y;
        }*//*

        static void UpdStrNoRef(ref string str)
        {
            Console.WriteLine(str);
        }


        static void Main(string[] args) {
            string str = "text";

            UpdStrClear(str); 
            Console.WriteLine($"After clean function {str} \n");

            UpdStrRef(ref str);
            Console.WriteLine($"After ref function {str} \n");

            UpdStrOut("wedwed", "fhasd", out str);
            Console.WriteLine($"from out function {str} \n");
        }
    }
}
*/